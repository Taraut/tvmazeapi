const path = require('path');
const express = require('express');

const app = express();

// eslint-disable-next-line no-undef
const publicDirectoryPath = path.join(__dirname, './public');
app.use(express.static(publicDirectoryPath));

// view engine setup
// eslint-disable-next-line no-undef
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile); // Visual Studio Code shows error but it works
app.set('view engine', 'html');

app.get('*', function(req, res) {
    res.render('index');
});

module.exports = app
