import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import { Row, Col } from 'antd';
import Header from './components/molecules/Header';
import MainPage from './components/main_page';
import NotFoundPage from './components/Not_found_page'

const AppRouter = () => (
    <BrowserRouter>
        <Row className="browserRouterMainRow">
            <Header />

            <Col span={2} />
            <Col span={20} className="mainPart">
                <nav>
                    <Switch>
                        <Route exact path="/" component={MainPage} />
                        <Route component={NotFoundPage} />
                    </Switch>
                </nav>
            </Col>
            <Col span={2} />
        </Row>
    </BrowserRouter>
);

export default AppRouter;
