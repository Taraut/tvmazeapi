import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow } from 'enzyme';
import MovieCard from './MovieCard';

Enzyme.configure({ adapter: new Adapter() });

describe('Search Input', () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        const item = {
            id: 12,
            name: "Lost Girl",
            summary: "<p><b>Lost Girl</b> follows supernatural seductres… her to solve a mystery, or to right a wrong.</p>",
            image: "http://static.tvmaze.com/uploads/images/medium_portrait/0/137.jpg"
        }
        ReactDOM.render(<MovieCard item={item} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('should render a img', () => {
        const onSearch = () => {};
        const item = {
            id: 12,
            name: "Lost Girl",
            summary: "<p><b>Lost Girl</b> follows supernatural seductres… her to solve a mystery, or to right a wrong.</p>",
            image: "http://static.tvmaze.com/uploads/images/medium_portrait/0/137.jpg"
        }
        const wrapper = shallow(<MovieCard item={item} />);
        expect(wrapper.find('img').prop('className')).toEqual('movieCard_img');
    });
});
