import React from 'react';
import PropTypes from 'prop-types';
import { Input } from 'antd';

const { Search } = Input;

class SearchInput extends React.Component {

    render() {

        const { className, placeholder, onSearch } = this.props;

        return (
            <div className={className}>
                <Search onSearch={onSearch} placeholder={placeholder} enterButton />
            </div>
        );
    }
}

SearchInput.propTypes = {
    className: PropTypes.string.isRequired,
    onSearch: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired
};
SearchInput.defaultProps = {
    className: 'showSearchInput',
    placeholder: 'Tv-show name'
};

export default SearchInput;
