import React from 'react';
import { Card } from 'antd';
import PropTypes from 'prop-types';

class MovieCard extends React.Component {
    render() {
        const { item, cardImageClassName } = this.props;

        return (
            <Card hoverable>
                <div className="movieCard_card">
                    <div>
                        {item.image && (
                            <img className={cardImageClassName} alt="No image" src={item.image} />
                        )}
                        {item.season && <div className="movieCard_descr"> Season: {item.season}</div>}
                        {item.number && <div className="movieCard_descr"> Episode: {item.number}</div>}
                    </div>
                    <div>
                        <div className="movieCard_name">{item.name}</div>
                        <div
                            className="movieCard_summary"
                            dangerouslySetInnerHTML={{ __html: item.summary }}
                        />
                    </div>
                </div>
            </Card>
        );
    }
}

MovieCard.propTypes = {
    item: PropTypes.object.isRequired,
    cardImageClassName: PropTypes.string
};
MovieCard.defaultProps = {
    cardImageClassName: 'movieCard_img'
};

export default MovieCard;
