import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { shallow } from 'enzyme';
import SearchInput from './SearchInput';

Enzyme.configure({ adapter: new Adapter() });

describe('Search Input', () => {
    it('should render without crashing', () => {
        const div = document.createElement('div');
        const onSearch = () => {};
        ReactDOM.render(<SearchInput onSearch={onSearch} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    it('should render a placeholder', () => {
        const onSearch = () => {};
        const placeholder = 'Test placeholder';
        const wrapper = shallow(<SearchInput onSearch={onSearch} placeholder={placeholder} />);
        expect(wrapper.find('Search').prop('placeholder')).toEqual(placeholder);
    });
});
