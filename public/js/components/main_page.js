import React from 'react';
import axios from 'axios';
import { Button } from 'antd';
import SearchInput from './atoms/SearchInput';
import MovieCard from './atoms/MovieCard';

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tvShows: [],
            tvShowsCopy: [],
            cardClickable: true,
            cardImageClassName: 'movieCard_img'
        };

        this.onSearch = this.onSearch.bind(this);
        this.movieCardOnClic = this.movieCardOnClic.bind(this);
        this.HorizontalMenu = this.HorizontalMenu.bind(this);
        this.backOnClick = this.backOnClick.bind(this);
    }

    movieCardOnClic(itemId) {
        if (!this.state.cardClickable) return;

        this.setState({
            cardClickable: false,
            cardImageClassName: 'movieCard_img-episode'
        });

        const apiUrl = `http://api.tvmaze.com/shows/${itemId}/episodes?specials=1`;

        axios
            .get(apiUrl)
            .then(res => {
                let reducedResData = [];
                if (res.data) {
                    const tvShowsData = res.data;
                    tvShowsData.forEach(tvShow => {
                        const reducedTvShowData = {
                            id: tvShow.id,
                            name: tvShow.name,
                            season: tvShow.season,
                            number: tvShow.numder,
                            summary: tvShow.summary,
                            image: tvShow.image ? tvShow.image.medium : false
                        };
                        reducedResData.push(reducedTvShowData);
                    });
                }

                this.setState({
                    tvShows: reducedResData
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    onSearch(value) {
        this.setState({
            cardClickable: true,
            cardImageClassName: 'movieCard_img'
        });
        const apiUrl = `http://api.tvmaze.com/search/shows?q=${value}`;

        axios
            .get(apiUrl)
            .then(res => {
                let reducedResData = [];
                if (res.data) {
                    const tvShowsData = res.data;
                    tvShowsData.forEach(tvShow => {
                        const reducedTvShowData = {
                            id: tvShow.show.id,
                            name: tvShow.show.name,
                            summary: tvShow.show.summary,
                            image: tvShow.show.image ? tvShow.show.image.medium : false
                        };
                        reducedResData.push(reducedTvShowData);
                    });
                }

                this.setState({
                    tvShows: reducedResData,
                    tvShowsCopy: reducedResData
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    backOnClick() {
        const { tvShowsCopy } = this.state;
        this.setState({
            tvShows: tvShowsCopy,
            cardImageClassName: 'movieCard_img',
            cardClickable: true
        });
    }

    HorizontalMenu() {
        const { cardClickable } = this.state;

        return (
            <div className="horizontalMenu">
                <SearchInput
                    onSearch={this.onSearch}
                    className="showSearchInput"
                    placeholder="Tv-show name"
                />

                {!cardClickable && <Button onClick={this.backOnClick}>Back</Button>}
            </div>
        );
    }

    render() {
        const { tvShows, cardImageClassName } = this.state;
        return (
            <div>
                <this.HorizontalMenu key='horMenuUp'/>

                {tvShows.length > 0 && (
                    <div>
                        {tvShows.map(item => (
                            <button
                                onClick={() => this.movieCardOnClic(item.id)}
                                key={item.id}
                                className="movieCard_button"
                            >
                                <MovieCard item={item} cardImageClassName={cardImageClassName} />
                            </button>
                        ))}

                        <this.HorizontalMenu />
                    </div>
                )}
            </div>
        );
    }
}

export default MainPage;
