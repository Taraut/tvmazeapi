import React from 'react';

const Header = () => {
    return (
        <div className="header">
            <div className="header_right-text">Simple tv-show search</div>
            <div className="header_left-text">TVMazeAPI</div>
        </div>
    );
};

export default Header;
