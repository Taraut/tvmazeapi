import React from 'react';
import { render } from 'react-dom';
import AppRouter from './app_router';

const jsx = <AppRouter />;

render(jsx, document.getElementById('root'));
